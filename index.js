
const addInputSelector = document.getElementById('addInput');
const addButtonSelector = document.getElementById('addButton');
const addContainerSelector = document.getElementById('appContainer');

createListFromStorage();

addButtonSelector.addEventListener('click', function () {
    if (addInputSelector.value.length === 0) {
        addInputSelector.classList.add('error');
        return;
    }
    addInputSelector.classList.remove('error');
    handleAddButton();
});


function createListFromStorage() {
    const storage = localStorage.getItem('data');
    const arrayStorage = JSON.parse(storage);

    if (!arrayStorage) { return; }

    for (const item of arrayStorage) {

        const listContainer = createListContainer(item.id);

        createCheckbox(item.id, listContainer, item.checked);

        createListName(item.id, listContainer, item.name);

        createEditButton(item.id, listContainer);

        createDeleteButton(item.id, listContainer);

    }

}

function handleAddButton() {
    const value = addInputSelector.value;

    const id = Math.random().toFixed(5).split('.')[1];

    const listContainer = createListContainer(id);

    createCheckbox(id, listContainer, false);

    createListName(id, listContainer, value);

    createEditButton(id, listContainer);

    createDeleteButton(id, listContainer);

    addInputSelector.value = "";

    saveData(id, value);

}

function createListContainer(id) {
    const listContainer = document.createElement('div');
    listContainer.classList.add('listContainer');
    addContainerSelector.appendChild(listContainer);
    listContainer.id = id;
    return listContainer;
}

function createCheckbox(id, listContainer, checked) {
    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.classList.add('checkboxElement');
    listContainer.appendChild(checkbox);
    checkbox.id = 'checkbox-' + id;
    checkbox.checked = checked;
    checkbox.addEventListener('change', function (event) {
        const storage = localStorage.getItem('data');
        const arrayStorage = JSON.parse(storage);
        const index = arrayStorage.findIndex(item => item.id === id);
        if (id !== -1) {
            arrayStorage[index].checked = event.target.checked;
            localStorage.setItem('data', JSON.stringify(arrayStorage));
        }
    });

}

function createListName(id, listContainer, value) {
    const elementName = document.createElement('span');
    elementName.classList.add('elementName');
    elementName.innerText = value;
    listContainer.appendChild(elementName);
    elementName.id = 'element-' + id;
}


function createEditButton(id, listContainer) {
    const editButton = document.createElement('button');
    editButton.classList.add('btn');
    editButton.classList.add('btn-primary');
    editButton.innerText = "Modifica";
    listContainer.appendChild(editButton);
    editButton.id = 'edit-' + id;
    editButton.onclick = function () {
        toggleElement(id, true);
        createEditInput(id, listContainer);
        createSaveButton(id, listContainer);
        createResetButton(id, listContainer);
    }

}

function createEditInput(id, listContainer) {
    const currentValue = document.getElementById('element-' + id);
    const editInput = document.createElement('input');
    editInput.id = 'edit-input-' + id;
    editInput.classList.add('form-control');
    editInput.value = currentValue.innerText;
    listContainer.appendChild(editInput);
}

function createSaveButton(id, listContainer) {
    const saveButton = document.createElement('button');
    saveButton.innerText = "Salva";
    saveButton.classList = "btn btn-primary";
    saveButton.id = 'save-' + id;
    listContainer.appendChild(saveButton);
    saveButton.onclick = function () {
        const editInput = document.getElementById('edit-input-' + id);
        const newValue = editInput.value;
        if (newValue.length === 0) {
            editInput.classList.add('error');
            return;
        }
        editInput.classList.remove('error');
        const storage = localStorage.getItem('data');
        const arrayStorage = JSON.parse(storage);
        const index = arrayStorage.findIndex(item => item.id === id);
        if (index !== -1) {
            arrayStorage[index].name = newValue;
            localStorage.setItem('data', JSON.stringify(arrayStorage));
            removeEditElements(id);
            toggleElement(id, false);
            const element = document.getElementById('element-' + id);
            element.innerText = newValue;
        }
    }
}

function createResetButton(id, listContainer) {
    const resetButton = document.createElement('button');
    resetButton.innerText = "Resetta";
    resetButton.classList = 'btn btn-primary';
    resetButton.id = 'reset-' + id;
    listContainer.appendChild(resetButton);
    resetButton.onclick = function () {
        removeEditElements(id);
        toggleElement(id, false);
    }
}

function removeEditElements(id) {
    const editInput = document.getElementById('edit-input-' + id);
    editInput.remove();

    const saveButton = document.getElementById('save-' + id);
    saveButton.remove();

    const resetButton = document.getElementById('reset-' + id);
    resetButton.remove();
}

function createDeleteButton(id, listContainer) {
    const deleteButton = document.createElement('button');
    deleteButton.classList = 'btn btn-primary'
    deleteButton.innerText = "Cancella";
    listContainer.appendChild(deleteButton);
    deleteButton.id = 'delete-' + id;

    deleteButton.onclick = function () {
        listContainer.remove();
        removeElement(id);
    }
}


function toggleElement(id, hidden) {
    const deleteButton = document.getElementById('delete-' + id);
    deleteButton.hidden = hidden;

    const editButton = document.getElementById('edit-' + id);
    editButton.hidden = hidden;

    const checkbox = document.getElementById('checkbox-' + id);
    checkbox.hidden = hidden;

    const elementName = document.getElementById('element-' + id);
    elementName.hidden = hidden;
}


function saveData(id, name) {
    const storage = localStorage.getItem('data');
    if (storage) {
        const oldStorage = JSON.parse(storage);
        oldStorage.push({ id: id, name: name, checked: false });
        localStorage.setItem('data', JSON.stringify(oldStorage));
    } else {
        const data = [{ id: id, name: name }];
        localStorage.setItem('data', JSON.stringify(data));
    }
}


function removeElement(id) {
    const storage = localStorage.getItem('data');
    if (storage) {
        const arrayStorage = JSON.parse(storage);
        console.log(arrayStorage);
        const index = arrayStorage.findIndex(item => item.id === id);
        if (index !== -1) {
            arrayStorage.splice(index, 1);
            localStorage.setItem('data', JSON.stringify(arrayStorage));
        }
    }
}



